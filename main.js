// no undeclared variables, etc
'use strict';

// let boxTop = 200;
// let boxLeft = 200;
let boxTop = parseInt(document.getElementById("box").offsetTop);
let boxLeft = parseInt(document.getElementById("box").offsetLeft);
const distance = 10;
// less expensive; stores in memroy; helps not regrabbing; if wnot chagne in html 
let box = document.getElementById("box");

function pressDownArrow() {
    boxTop = boxTop + distance;
    box.style.top = boxTop + "px";
}
function pressUpArrow() {
    boxTop = boxTop - distance;
    box.style.top = boxTop + "px";
}
function pressLeftArrow() {
    boxLeft = boxLeft - distance;
    box.style.left = boxLeft + "px";
}
function pressRightArrow() {
    boxLeft = boxLeft + distance;
    box.style.left = boxLeft + "px";
}

document.addEventListener('keydown', (event) => {
    const keyName = event.key;
    switch (keyName) {
        case "ArrowDown":
            pressDownArrow();
            break;
        case "ArrowUp":
            pressUpArrow();
            break;
        case "ArrowLeft":
            pressLeftArrow();
            break;
        case "ArrowRight":
            pressRightArrow();
            break;
    }
    console.log('keydown event\n\n' + 'key: ' + keyName + "\n\n" + box.style.top, box.style.left);
});